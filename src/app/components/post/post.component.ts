import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';

import { IformPost } from './../models/IformPost';
import { ServipostService } from './../../services/servipost.service';


@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})

export class PostComponent implements OnInit {

  public formPost: FormGroup | any = null;
  // tslint:disable-next-line: no-inferrable-types
  public submitted: boolean = false;

  constructor(private formBuilder: FormBuilder, private servipostService: ServipostService) {

    this.formPost = this.formBuilder.group({

      title: ['', [Validators.required, Validators.minLength(2)]],
      name: ['', [Validators.required, Validators.minLength(2)]],
      email: ['', [Validators.required, Validators.email]],
      age: ['', [Validators.required, Validators.minLength(2)]],
      image: ['', [Validators.required, Validators.minLength(10)]],
      description: ['', [Validators.required, Validators.minLength(10)]],

    });
  }

  ngOnInit(): void {/* empty */}

  public onSubmit(): void {

    this.submitted = true;

    if (this.formPost.valid) {
      const responseFormPost: IformPost = {
        title: this.formPost.get('title').value,
        name: this.formPost.get('name').value,
        email: this.formPost.get('email').value,
        age: this.formPost.get('age').value,
        image: this.formPost.get('image').value,
        description: this.formPost.get('description').value,
      };
      this.servipostService.postFormPost(responseFormPost);
      console.log(responseFormPost);
      this.formPost.reset();
      this.submitted = false;
    }
  }
}
