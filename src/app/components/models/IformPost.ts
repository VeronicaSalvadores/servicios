import { EmailValidator } from '@angular/forms';

export interface IformPost{
    title: string;
    name: string;
    email: string;
    age: number;
    image: string;
    description: string;
}
