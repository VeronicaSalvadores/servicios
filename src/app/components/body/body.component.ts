import { IformPost } from './../models/IformPost';
import { ServipostService } from './../../services/servipost.service';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.scss']
})

export class BodyComponent implements OnInit {

  public myPosts: IformPost[] | any = null;

  constructor(private servipostService: ServipostService) {}

  ngOnInit(): void {
    this.myPosts = this.servipostService.getFormPost();

  }

}
