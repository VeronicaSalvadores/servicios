import { Injectable } from '@angular/core';

import { IformPost } from './../components/models/IformPost';

@Injectable({
  providedIn: 'root'
})
export class ServipostService {


  private posteo: IformPost[] | any;

  constructor() {

    this.posteo = [{
      title: 'Mi primer titulo',
      name: 'Verónica',
      email: 'vero@email.com',
      age: 31,
      image: 'https://i.blogs.es/aa1b9a/luna-100mpx/450_1000.jpg',
      description: 'Esto es una descripción...',
    }];

  }

  getFormPost(): IformPost[] {
    return this.posteo;
  }

  postFormPost(posteo: IformPost): void  {
    this.posteo.push(posteo);
  }

  deleteItems(index: any): void {
    const id = 0;
    this.posteo = this.posteo.splice(id,  1);
  }

}
