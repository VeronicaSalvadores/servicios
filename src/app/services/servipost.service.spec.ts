import { TestBed } from '@angular/core/testing';

import { ServipostService } from './servipost.service';

describe('ServipostService', () => {
  let service: ServipostService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ServipostService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
