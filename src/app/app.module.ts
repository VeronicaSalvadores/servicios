import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BodyComponent } from './components/body/body.component';
import { PostComponent } from './components/post/post.component';
import { ServipostService } from './services/servipost.service';

@NgModule({
  declarations: [
    AppComponent,
    BodyComponent,
    PostComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule
  ],
  providers: [ServipostService],
  bootstrap: [AppComponent]
})
export class AppModule { }
